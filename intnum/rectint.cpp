#include <iostream>
#include <stdlib.h>

/*Integracion rectangular*/

double funcion (double);

int main ()
{
	double a, b, h, vx1, vx, sum, vf, vi, i;
	
	std::cout << " Ingrese el limite inferior: " << std::endl;
	std::cin >> a ;
	std::cout << " Ingrese el limite superior: " << std::endl;
	std::cin >> b ;
	std::cout << "Ingrese el valor de h: " << std::endl;
	std::cin >> h ;

	sum = 0;
	vx1 = h/2 + a ;

	for (i = vx1; i < b; i += h){

		vx = i;
		vf = funcion(vx);
		sum += vf;
	}

	vi = h * sum;

	std::cout << " El valor de la integral es: " << vi << std::endl;
	return 0;
}


double funcion (double vx){

	double vf;

	vf = vx * vx ;
	
	return(vf);
}

