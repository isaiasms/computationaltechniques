#include <iostream>
#include <stdlib.h>
using std::cout;
using std::cin;
using std::endl;

/*Integracion trapezoidal*/

double funcion (double);

int main (){
	double a, b, h, vx, sum, vf, vf1, vfn, vi, i;
	
	cout << "Ingrese el limite inferior: " << endl;
	cin >> a ;
	cout << "Ingrese el limite superior: " << endl;
	cin >> b;
	cout << "Ingrese el valor de h: " << endl;
	cin >> h;
	
	vf1= funcion(a);
	vfn= funcion(b);
	sum = 0;

	for (i = a + h; i < (b - h); i += h){

		vx= i;
		vf = funcion(vx);
		sum += vf;
	}

	vi = (h/2 * (vf1 + vfn)) + (h * sum);

	cout << "El valor de la integral es: " << vi << endl;
}


double funcion (double vx){

	double vf;

	vf = vx * vx ;
	
	return(vf);
}


