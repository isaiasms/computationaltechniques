#include <iostream>
#include <cstdlib>
#include <cmath>
using std::cout;
using std::cin;
using std::endl;
using std::cerr;
#include "trapint.h"

trapezoidalintegrator::trapezoidalintegrator (){
	a = 0.0 ; b = 1.0 ; n = 100;
}
trapezoidalintegrator::trapezoidalintegrator(double usra, double usrb, int usrn){
	a=usra; b=usrb; n=usrn;
}

double trapezoidalintegrator::compute(){


	double h, sum, vf, vf1, vfn, vi, vx;
/*
	cout << "Ingrese el límite inferior: " << endl;
	cin >> usra;
	cout << "Ingrese el límite superior: " << endl;
	cin >> usrb;
	cout << "Ingrese el número de rectangulos: " << endl;
	cin >> usrn;
*/	
	h = (b - a)/n;

	vf1 = function (a);
	vfn = function (b);
	sum = 0;

	for (double i = a + h ; i < (b-h); i += h){

		vx = i;
		vf = function (vx);
		sum += vf;

	}

	vi = (h/2 * (vf1 + vfn)) + (h * sum);

	return (vi);
}

double trapezoidalintegrator::function(double x){
	return(x*x);
}
	
