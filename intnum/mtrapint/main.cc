#include <iostream>
#include <cstdlib>
using std::cout;
using std::cin;
using std::endl;
using std::cerr;
#include "trapint.h"

/*Integracion trapezoidal*/

int main (){
	trapezoidalintegrator integrate(-2.0,5.0,50);
	cout << "result: " << integrate.compute() << endl;
	return EXIT_SUCCESS;
}


