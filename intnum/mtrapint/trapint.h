#ifndef _TRAPINTEGRATOR_H_
#define _TRAPINTEGRATOR_H_

class trapezoidalintegrator {
public:
	trapezoidalintegrator();
	trapezoidalintegrator(double, double, int);
	double compute ();
	double function (double);
protected:
	double a, b;
	int n;
};

#endif 

