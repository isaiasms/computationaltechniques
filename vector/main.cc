#include <cstdlib>
#include <iostream>
using namespace std;
#include "mycomplex.h"

int main (int argc, char *argv[])
{
	Mycomplex a(1,2), b(3,4);
	cout << "a " << a << ", b= " << b << endl;
	cout << "a*b= " << (a*b) << endl;
	cout << "a+b= " << (a+b) << endl;
	cout << "4b=  " << (b*4.0e0) << endl;
	
	return EXIT_SUCCESS;
}
