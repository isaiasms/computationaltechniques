#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

int main (void){
	int N=1000;
	vector<int> vec(N);
	for (int i=0 ; i<vec.size() ; ++i){
		vec[i]=i+1;
	}
	long int sum=0;
	for (int i=0 ; i<vec.size() ; ++i){
		sum+=vec[i];
	}
	cout << "sum: " << sum << ". \nIt should be: " << (N*(N+1)/2) << endl;
	return EXIT_SUCCESS;
}
