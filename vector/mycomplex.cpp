#include <cstdlib>
#include <iostream>
#include "mycomplex.h"
Mycomplex::Mycomplex() {
	re=im=0.0e0;
}
Mycomplex::Mycomplex(double r, double i)
{
	re=r;
	im=i;
}
Mycomplex Mycomplex::operator*(const Mycomplex &other) const {
	double a=re, b=im;
	double c=other.re, d=other.im;
	return Mycomplex((a*c-b*d),(a*d+b*c));
}
Mycomplex Mycomplex::operator+(const Mycomplex &other) const {
	double a=re, b=im;
	double c=other.re, d=other.im;
	return Mycomplex((a+c),(b+d));
}
Mycomplex Mycomplex::operator*(const double &scalar) const {
	double a=re,b=im;
	return Mycomplex((a*scalar),(b*scalar));
}
Mycomplex& Mycomplex::operator=(const Mycomplex& rhs){
	if (this != &rhs){
		re=rhs.re;
		im=rhs.im;
		return *this;
	}
}

std::ostream& operator<<(std::ostream &out, const Mycomplex &a){
	return out << "(" << a.Re() << "," << a.Im() << "i)" ;
}
