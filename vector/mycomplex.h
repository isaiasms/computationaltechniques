#include <iostream>

class Mycomplex {
	public:
		Mycomplex ();
		Mycomplex (double r, double i);
		Mycomplex (const Mycomplex &other);
		double Re (void) const {return re;}
		double Im (void) const {return im;}
		Mycomplex operator *(const Mycomplex &other) const;
		Mycomplex operator +(const Mycomplex &other) const;
		Mycomplex operator *(const double &scalar) const;
		Mycomplex& operator =(const Mycomplex& rhs);
	protected:
		double re, im;
};

std::ostream& operator << (std::ostream& out, const Mycomplex &a);
