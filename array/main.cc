# include <cstdlib>
# include <iostream>
using namespace std;
# include "myarray.h"

int main ( int argc , char * argv []) {
	MyArray array (5) ;
	for ( int i=0 ; i< array . Dim () ; ++i ) {
		array.SetValue (i ,2.5e0*double (i +1) );
	}
	for ( int j=0 ; j< array . Dim () ; ++j ) {
		cout << " index : " << j << ", value : " << array.GetValue (j) << endl ;
	}
	return EXIT_SUCCESS;
}
