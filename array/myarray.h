# ifndef _MYARRAY_H_
# define _MYARRAY_H_
class MyArray {
	public :
		MyArray ( void );
		MyArray (int n);
		~MyArray ();
		int Dim () { return dim ;}
		void SetValue ( int idx , double val );
		double GetValue (int idx );
	protected :
		double *array ;
		int dim ;
};
# endif /* _MYARRAY_H_ */
