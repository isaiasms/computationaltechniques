# include <cstdlib>
# include <iostream>
using namespace std;
# include "myarray.h"

	MyArray::MyArray() {
		dim =0;
		array = NULL;
	}	
	MyArray::MyArray (int n) {
		dim =n;
		array = new double [ dim ];
	}
	MyArray::~MyArray () {
		dim =0;
		delete [] array;
	}
	void MyArray::SetValue ( int idx , double val) {
		if ( idx >= dim ) {
			cout << " Error : array is too small " << endl ;
		return ;
		} else if ( idx <0 ) {
			cout << " The index cannot be negative !" << endl;
			return ;
		} else {
			array [idx ]= val ;
			return ;
		}
	}
	double MyArray::GetValue ( int idx) {
		if ( idx >= dim ) {
			cout << " Error : this value does not exist !" << endl ;
			return 0.0e0;
		} else if ( idx <0 ) {
			cout << " The index cannot be negative !" << endl;
			return 0.0e0;
		} else {
			return array [ idx ];
		}
	}
