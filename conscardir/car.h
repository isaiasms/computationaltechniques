#include <string>
using std::string;

class Car {
public:
	Car (string aName);
	void start (void);
	string GetName (void) {return name;}
private:
	string name;
};

