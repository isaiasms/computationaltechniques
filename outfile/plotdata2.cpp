#include <cstdlib>
#include <iostream>
# include <fstream>
# include <string>
# include <cmath>
using namespace std;

int main (int argc, char *argv[])
{
	if ( argc<4 ){
		cout << "Not enough arguments" << endl;
		return EXIT_FAILURE;
	}
	double xmin=stod(string(argv[1]));
	double xmax=stod(string(argv[2]));
	int n=stod(string(argv[3]));
	cout << "xmin: " << xmin << endl;
	cout << "xmax: " << xmax << endl;
	cout << "n: " << n << endl;
	char name []= "data.dat";
	ofstream ofile (name, ios::out);
	double xi=xmin ,dx = (xmax - xmin)/double (n-1);
	for ( int i=0 ; i<n ; ++i ) {
		ofile << (xi) << " " << (sin(xi)/xi) << endl ;
		xi += dx;
	}
	ofile.close () ;
	return EXIT_SUCCESS ;
}

