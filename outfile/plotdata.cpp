#include <cstdlib>
#include <iostream>
# include <fstream>
# include <string>
# include <cmath>
using namespace std;

int main (){
	double xmin , xmax ;
	int n;
	cout << " Enter a pair xmin , xmax : ";
	cin >> xmin >> xmax ;
	cout << " Now enter a number of points : ";
	cin >> n;
	char name []= "data.dat";
	ofstream ofile (name, ios::out);
	double xi=xmin ,dx = (xmax - xmin)/double (n-1);
	for ( int i=0 ; i<n ; ++i ) {
		ofile << (xi) << " " << (sin(xi)/xi) << endl ;
		xi += dx;
	}
	ofile.close () ;
	return EXIT_SUCCESS ;
	}

